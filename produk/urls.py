from django.urls import path
from .views import produk

app_name = 'produk'

urlpatterns = [
    path('', produk, name='home'),
]
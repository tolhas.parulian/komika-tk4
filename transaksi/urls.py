from django.urls import path
from .views import transaksi

app_name = 'transaksi'

urlpatterns = [
    path('', transaksi, name='home'),
]
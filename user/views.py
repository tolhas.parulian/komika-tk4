from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from hashlib import sha3_512 as sha

def auth_login(request):
    asd = ''
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        hashed = sha(password.encode()).hexdigest()
        print(hashed)
    with connection.cursor() as anggota:
        anggota.execute("SELECT * FROM anggota")
        asd = anggota.fetchall()
    return render(request, 'login.html', {'asd' : asd})
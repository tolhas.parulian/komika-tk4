from django.urls import path
from .views import auth_login

app_name = 'user'

urlpatterns = [
    path('login/', auth_login, name='login'),
]